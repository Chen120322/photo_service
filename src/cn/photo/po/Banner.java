package cn.photo.po;

import cn.photo.core.BaseModel;

@SuppressWarnings("serial")
public class Banner extends BaseModel{

    private String banner;

    private String bannerUrl;


    public String getBanner() {
        return banner;
    }

    public void setBanner(String banner) {
        this.banner = banner == null ? null : banner.trim();
    }

    public String getBannerUrl() {
        return bannerUrl;
    }

    public void setBannerUrl(String bannerUrl) {
        this.bannerUrl = bannerUrl == null ? null : bannerUrl.trim();
    }

}