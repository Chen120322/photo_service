package cn.photo.po;


/**
 * 用户的包装类
 * @author Administrator
 *
 */
public class UserQueryVo {

	private User user;
	
	private UserCustom uc;

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public UserCustom getUc() {
		return uc;
	}

	public void setUc(UserCustom uc) {
		this.uc = uc;
	}
	
	
}
