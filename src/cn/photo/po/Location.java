package cn.photo.po;

import cn.photo.core.BaseModel;

@SuppressWarnings("serial")
public class Location extends BaseModel{
	
	private String name;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	

}
