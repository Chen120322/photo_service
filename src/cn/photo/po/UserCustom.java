package cn.photo.po;


/**
 * 用户扩展类
 * @author Administrator
 *
 */
public class UserCustom extends User{

	//可以添加一些扩展属性
	
	private Integer count;//粉丝数量

	public Integer getCount() {
		return count;
	}

	public void setCount(Integer count) {
		this.count = count;
	}
	
}
