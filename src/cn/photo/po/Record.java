package cn.photo.po;

import java.util.Date;

import cn.photo.core.BaseModel;

@SuppressWarnings("serial")
public class Record extends BaseModel{

    private Long photoalbumId;

    private Long userId;

    private Integer type;

    

    public Long getPhotoalbumId() {
        return photoalbumId;
    }

    public void setPhotoalbumId(Long photoalbumId) {
        this.photoalbumId = photoalbumId;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

   
}