package cn.photo.po;

import cn.photo.core.BaseModel;

@SuppressWarnings("serial")
public class Photoalbum extends BaseModel{
    
    private String name;

    private Integer userId;

    private boolean available;
    
    private String location;
    
    public boolean isAvailable() {
		return available;
	}

	public void setAvailable(boolean available) {
		this.available = available;
	}
    private String cover;
    public String getCover() {
		return cover;
	}

	public void setCover(String cover) {
		this.cover = cover;
	}


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name == null ? null : name.trim();
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}
    
}