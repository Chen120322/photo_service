package cn.photo.po;

import cn.photo.core.BaseModel;


@SuppressWarnings("serial")
public class User extends BaseModel{

    private String name;

    private String mobile;

    private String token;

    private String sessionKey;

    private Integer type;

    private String openid;

    private String avatarurl;

    private String jobTitle;
    
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name == null ? null : name.trim();
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile == null ? null : mobile.trim();
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token == null ? null : token.trim();
    }

    public String getSessionKey() {
        return sessionKey;
    }

    public void setSessionKey(String sessionKey) {
        this.sessionKey = sessionKey == null ? null : sessionKey.trim();
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public String getOpenid() {
        return openid;
    }

    public void setOpenid(String openid) {
        this.openid = openid == null ? null : openid.trim();
    }

    public String getAvatarurl() {
        return avatarurl;
    }

    public void setAvatarurl(String avatarurl) {
        this.avatarurl = avatarurl == null ? null : avatarurl.trim();
    }

    
	public String getJobTitle() {
		return jobTitle;
	}

	public void setJobTitle(String jobTitle) {
		this.jobTitle = jobTitle;
	}

	public User( String name, String mobile, String token,
			String sessionKey, Integer type, String openid, String avatarurl) {
		super();
	
		this.name = name;
		this.mobile = mobile;
		this.token = token;
		this.sessionKey = sessionKey;
		this.type = type;
		this.openid = openid;
		this.avatarurl = avatarurl;
	}

	public User() {
		super();
		// TODO Auto-generated constructor stub
	}
    
}