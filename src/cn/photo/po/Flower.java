package cn.photo.po;

import java.util.Date;

public class Flower {
    private Long id;

    private Long tUserId;

    private Long pUserId;

    private Date createdate;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long gettUserId() {
        return tUserId;
    }

    public void settUserId(Long tUserId) {
        this.tUserId = tUserId;
    }

    public Long getpUserId() {
        return pUserId;
    }

    public void setpUserId(Long pUserId) {
        this.pUserId = pUserId;
    }

    public Date getCreatedate() {
        return createdate;
    }

    public void setCreatedate(Date createdate) {
        this.createdate = createdate;
    }
}