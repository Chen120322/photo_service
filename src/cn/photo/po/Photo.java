package cn.photo.po;

import cn.photo.core.BaseModel;

@SuppressWarnings("serial")
public class Photo extends BaseModel{

    private String photoUrl;

    private Long photoalbumId;

    private Integer type;
    
    public String getPhotoUrl() {
        return photoUrl;
    }

    public void setPhotoUrl(String photoUrl) {
        this.photoUrl = photoUrl == null ? null : photoUrl.trim();
    }

    public Long getPhotoalbumId() {
        return photoalbumId;
    }

    public void setPhotoalbumId(Long photoalbumId) {
        this.photoalbumId = photoalbumId;
    }

	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}
}