package cn.photo.utils;

import java.text.SimpleDateFormat;
import java.util.Calendar;

public class DateUtil {
	
	/**
	 * 上个月末时间
	 */
	public String lastMonthDate(){
		// java 代码如何获取当前时间的上一个月的月末时间.. 
		Calendar cal = Calendar.getInstance();
		// 设置当月的第几天开始。0表示上个月末
		cal.set(Calendar.DAY_OF_MONTH, 0); 
		//格式化输出年月日 
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd"); 

		String time = format.format(cal.getTime());
		return time;
	}
	/**
	 * 上周日时间
	 * @return
	 */
	public String lastWeekDate(){
	    Calendar cal =Calendar.getInstance();
	    cal.set(Calendar.DAY_OF_WEEK,1);
	   // cal.add(Calendar.DATE, -7);
	    SimpleDateFormat format =  new SimpleDateFormat("yyyy-MM-dd");
	    String time = format.format(cal.getTime());
	    return time;
	}

}
