package cn.photo.utils;

import java.io.File;
import java.util.UUID;

import com.google.gson.Gson;
import com.qiniu.common.QiniuException;
import com.qiniu.http.Response;
import com.qiniu.storage.UploadManager;
import com.qiniu.storage.model.DefaultPutRet;
import com.qiniu.util.Auth;

/**
 * 七牛云工具类
 * @author Administrator
 *
 */
public class QiNiu {
	public static String uploadQiniu(File file,String fileName){
		/* ---------------------------------------- 七牛云上传图片     开始  ------------------------------*/		
		
		
		//图片上传改动
		String key="";
	
		//...其他参数参考类注释
		UploadManager uploadManager = new UploadManager();
		
		//默认不指定key的情况下，以文件内容的hash值作为文件名	 
		key = UUID.randomUUID().toString().replaceAll("-", "")+fileName.substring(fileName.lastIndexOf("."), fileName.length());
		Auth auth = Auth.create(Constants.QINIU_ACCESS_Key, Constants.QINIU_SECRET_KEY);
		String upToken = auth.uploadToken(Constants.QINIU_BUCKET);
		try {
				Response response = uploadManager.put(file,key,upToken);
				//解析上传成功的结果
				DefaultPutRet putRet = new Gson().fromJson(response.bodyString(), DefaultPutRet.class);
				System.out.println(putRet.key);
				System.out.println(putRet.hash);
			} catch (QiniuException ex) {
				Response r = ex.response;
				System.err.println(r.toString());
				
				try {
				       System.err.println(r.bodyString());
				    } catch (QiniuException ex2) {
				        //ignore
				    }
			}
				
				
				
		return Constants.QINIU_PHOTO_URL+key;
		
		/* ---------------------------------------- 七牛云上传图片     结束  ------------------------------*/
	}

}
