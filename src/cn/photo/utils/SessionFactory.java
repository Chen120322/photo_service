package cn.photo.utils;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

public class SessionFactory {
    private static volatile HttpSession session = null;  
    private SessionFactory(){}  
    public static HttpSession getSession(HttpServletRequest request) {  
        if (session == null) {  
            synchronized (SessionFactory.class) {  
                if (session == null) {  
                    session = request.getSession();  
                }  
            }  
        }  
        return  session;  
    }  
	
}
