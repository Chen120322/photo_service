package cn.photo.utils;

import java.lang.management.ManagementFactory;
import java.net.InetAddress;
import java.net.NetworkInterface;

import org.apache.commons.lang.StringUtils;
import org.apache.ibatis.logging.Log;
import org.apache.ibatis.logging.LogFactory;

/**
 * 生成数据库主键Id工具类
 * @author fyc
 *
 */
public class Sequence {
	private static final Log logger = LogFactory.getLog(Exception.class);
	
	private final long twepoch = 1288834974657L;
	private long lastTimestamp = -1L;
	private long sequence = 0L;
	
	private final long workerIdBits = 5L;
	private final long datacenterIdBits = 5L;
	private final long sequenceBits = 12L;
	private final long sequenceMask = -1L ^ (-1L << sequenceBits);
	private final long timestampLeftShift = sequenceBits + workerIdBits + datacenterIdBits;
	private final long maxDatacenterId = -1L ^ (-1L << datacenterIdBits);
	private final long datacenterId = getDatacenterId(maxDatacenterId);
	private final long maxWorkerId = -1L ^ (-1L << workerIdBits);
	private long workerId = getMaxWorkerId(datacenterId, maxWorkerId);
	private final long datacenterIdShift = sequenceBits + workerIdBits;
	private final long workerIdShift = sequenceBits;
	private long now = System.currentTimeMillis();
	public  synchronized  long nextId(){
		Long timestamp = now;
		if (lastTimestamp == timestamp) {
			sequence = (sequence + 1) & sequenceMask;
			if (sequence == 0) {
				timestamp = tilNextMillis(lastTimestamp);
			}
		} else {
			sequence = 0L;
		}

		lastTimestamp = timestamp;
		
		return ((timestamp - twepoch) << timestampLeftShift) | (datacenterId << datacenterIdShift) | (workerId << workerIdShift)
				| sequence;
	}
	
	protected long tilNextMillis(long lastTimestamp) {
		long timestamp = now;
		while (timestamp <= lastTimestamp) {
			timestamp = now;
		}
		return timestamp;
	}

	protected static long getDatacenterId(long maxDatacenterId) {
		long id = 0L;
		try {
			InetAddress ip = InetAddress.getLocalHost();
			NetworkInterface network = NetworkInterface.getByInetAddress(ip);
			if (network == null) {
				id = 1L;
			} else {
				byte[] mac = network.getHardwareAddress();
				id = ((0x000000FF & (long) mac[mac.length - 1]) | (0x0000FF00 & (((long) mac[mac.length - 2]) << 8))) >> 6;
				id = id % (maxDatacenterId + 1);
			}
		} catch (Exception e) {
			logger.warn(" getDatacenterId: " + e.getMessage());
		}
		return id;
	}
	
	protected static long getMaxWorkerId(long datacenterId, long maxWorkerId) {
		StringBuffer mpid = new StringBuffer();
		mpid.append(datacenterId);
		String name = ManagementFactory.getRuntimeMXBean().getName();
		if (StringUtils.isNotEmpty(name)) {
			
			mpid.append(name.split("@")[0]);
		}
		
		return (mpid.toString().hashCode() & 0xffff) % (maxWorkerId + 1);
	}
	
}
