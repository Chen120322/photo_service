/**
 * 
 */
package cn.photo.core;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import cn.photo.core.Constants;
import cn.photo.core.ReturnObject;
import cn.photo.core.BaseException;
import cn.photo.core.IllegalParameterException;
import cn.photo.core.HttpCode;
import org.springframework.http.ResponseEntity;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.ModelAndView;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.serializer.SerializerFeature;

/**
 * 控制器基类
 */
public abstract class BaseController {
    protected final Logger logger = LogManager.getLogger(this.getClass());

    /** 获取当前用户Id */
//    protected Long getCurrUser() {
//        return WebUtil.getCurrentUser();
//    }

    /** 设置成功响应代码 */
    protected ResponseEntity<ModelMap> setSuccessModelMap(ModelMap modelMap) {
        return setSuccessModelMap(modelMap, null);
    }

    /** 设置成功响应代码 */
    protected ResponseEntity<ModelMap> setSuccessModelMap(ModelMap modelMap, Object data) {
        return setModelMap(modelMap, HttpCode.OK, data);
    }

    /** 设置响应代码 */
    protected ResponseEntity<ModelMap> setModelMap(ModelMap modelMap, HttpCode code) {
        return setModelMap(modelMap, code, null);
    }

    /** 设置响应代码 */
    protected ResponseEntity<ModelMap> setModelMap(ModelMap modelMap, HttpCode code, Object data) {
        modelMap.remove("void");
        if (data != null) {
            modelMap.put("data", data);
        }
        modelMap.put("code", code.value());
        modelMap.put("msg", "请求成功");//code.msg()

        modelMap.put("timestamp", System.currentTimeMillis());
        return ResponseEntity.ok(modelMap);
    }
    /** 设置响应代码 */
    protected ModelAndView setModelAndView(ModelAndView view, HttpCode code, Object data) {
        if (data != null) {
            view.addObject("data", data);
        }
        view.addObject("code", code.value());
        view.addObject("msg", "请求成功");//code.msg()
        
        view.addObject("timestamp", System.currentTimeMillis());
        return view;
    }

    /** 异常处理 */
    @ExceptionHandler(Exception.class)
    public void exceptionHandler(HttpServletRequest request, HttpServletResponse response, Exception ex)
            throws Exception {
        logger.error(Constants.Exception_Head, ex);
        ModelMap modelMap = new ModelMap();
        if (ex instanceof BaseException) {
            ((BaseException) ex).handler(modelMap);
        } else if (ex instanceof IllegalArgumentException) {
            new IllegalParameterException(ex.getMessage()).handler(modelMap);
        } else if (ex instanceof RuntimeException) {
            if(ex.getMessage().length() > 30) {
                setModelMap(modelMap, HttpCode.INTERNAL_SERVER_ERROR);
            }else {
                modelMap.put("code", HttpCode.INTERNAL_SERVER_ERROR.value());
                modelMap.put("msg", ex.getMessage());
                modelMap.put("timestamp", System.currentTimeMillis());
            }
        } else {
            setModelMap(modelMap, HttpCode.INTERNAL_SERVER_ERROR);
        }
        request.setAttribute("msg", modelMap.get("msg"));
        ReturnObject ro = new ReturnObject();
        ro.setSuccess(false);
        ro.setMessage((String) modelMap.get("msg"));
        modelMap.put("data", ro);
        response.setContentType("application/json;charset=UTF-8");
        byte[] bytes = JSON.toJSONBytes(modelMap, SerializerFeature.DisableCircularReferenceDetect);
        response.getOutputStream().write(bytes);
    }
}
