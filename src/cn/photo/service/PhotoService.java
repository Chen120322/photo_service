package cn.photo.service;

import cn.photo.po.Photoalbum;

import com.github.pagehelper.PageInfo;

public interface PhotoService {
	
	public PageInfo queryPhotoByAlbumId(Photoalbum photoalbum,int pageNum,int pageSize);

}
