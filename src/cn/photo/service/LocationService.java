package cn.photo.service;

import java.util.List;

import cn.photo.po.Location;

public interface LocationService {
	
	public List<Location> queryAllLocation();

}
