package cn.photo.service;

import cn.photo.po.Record;

public interface RecordService {
	
	public Integer insert(Record record);
	
	public Integer queryPraise(Record record);
	
	public Integer deletePraise(Record record);

}
