package cn.photo.service;

import com.github.pagehelper.PageInfo;

public interface BannerService {
	
	public PageInfo queryBanner(Integer pageNum, Integer pageSize);

}
