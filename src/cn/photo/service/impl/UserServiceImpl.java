package cn.photo.service.impl;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import net.sf.json.JSONObject;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;

import sun.misc.BASE64Decoder;
import cn.photo.mapper.UserMapper;
import cn.photo.po.Photoalbum;
import cn.photo.po.User;
import cn.photo.po.UserCustom;
import cn.photo.service.UserService;
import cn.photo.utils.Constants;
import cn.photo.utils.DateUtil;
import cn.photo.utils.HttpUtils;

/**
 * 用户信息
 * @author Administrator
 *
 */
@Service
public class UserServiceImpl implements UserService{
	
	@Autowired
	private UserMapper um;
	
	private User user = new User();
	
	private HttpUtils http = new HttpUtils();

	@Override
	public UserCustom findUserInfo(UserCustom uc){
		// TODO Auto-generated method stub
		return um.findUserInfo(uc);
	}

	@Override
	public String findToken(String code) throws Exception{
		
		String url = Constants.WEIBO_URL+"oauth2/access_token";
		Map<String,String> map = new HashMap<String,String>();
		map.put("client_id", Constants.WEIBO_APPID);
		map.put("client_secret", Constants.WEIBO_SECRET);
		map.put("grant_type", "authorization_code");
		map.put("code", code);
		map.put("redirect_uri", Constants.REDIRECT_URL);
		String res = http.doPost(url, map);
		if(res == null || "".equals(res)){
			return null;
		}
		//获取token
		JSONObject json = JSONObject.fromObject(res);
		String token = json.getString("access_token");
		String uid = json.getString("uid");
		
		UserCustom uc = new UserCustom();
		uc.setOpenid(uid);
		uc = findUserInfo(uc);
		if((uc != null && !"".equals(uc.getOpenid())) && uc.getOpenid().equals(uid)){
			json.put("user", uc);
			return json.toString();
		}
		res = findUser(token,uid);
		if(res == null || "".equals(res)){
			return null;
		}
		//获取用户信息
		json = JSONObject.fromObject(res);
		if(json == null){
			return null;
		}
		
		user.setName(json.getString("name"));
		user.setAvatarurl(json.getString("profile_image_url"));
		user.setOpenid(uid);
		user.setToken(token);
		user.setCreateTime(new Date());
		user.setUpdateTime(new Date());
		//saveUserInfo(user);
		json.put("user", user);
		return json.toString();
	}

	@Override
	public String findUser(String token,String uid) throws Exception{
		String url = Constants.WEIBO_URL+"2/users/show.json?access_token="+token+"&uid="+ uid;
		String res = http.doGet(url);
		return res;
	}

	@Override
	public void saveUserInfo(UserCustom uc){
		um.insert(uc);
	}

	@Override
	public String findWXLogin(String code) throws Exception {
		String url = Constants.WX_URL+"sns/jscode2session";
		Map<String,String> map = new HashMap<String,String>();
		map.put("appid", Constants.WX_APPID);
		map.put("secret", Constants.WX_SECRET);
		map.put("js_code", code);
		map.put("grant_type", "authorization_code");
		String res = http.doPost(url, map);
		return res;
	}

	@Override
	public void updateUserInfo(User user) throws Exception {
		um.updateByPrimaryKeySelective(user);
	}

	@Override
	public User findPhotoUserInfo(Photoalbum pa) {
		// TODO Auto-generated method stub
		return um.findPhotoUserInfo(pa);
	}

	@Override
	public PageInfo findLastTopUserInfo(String time,Integer pageNum,Integer pageSize) {
		PageHelper.startPage(pageNum, pageSize, true);
		List<UserCustom> list = um.findLastTopUserInfo(time);
		PageInfo page = new PageInfo(list);
		return page;
	}

}
