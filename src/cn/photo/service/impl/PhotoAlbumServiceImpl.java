package cn.photo.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cn.photo.mapper.PhotoalbumMapper;
import cn.photo.po.Photoalbum;
import cn.photo.service.PhotoAlbumService;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;

@Service
public class PhotoAlbumServiceImpl implements PhotoAlbumService {
	
	@Autowired
	private PhotoalbumMapper pb;

	@Override
	public PageInfo queryCover(Integer pageNum,Integer pageSize,Photoalbum pa) {
		PageHelper.startPage(pageNum, pageSize, true);
		List<Photoalbum> list = pb.queryCover(pa);
		PageInfo page = new PageInfo(list);
		return page;
	}

}
