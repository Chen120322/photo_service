package cn.photo.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cn.photo.mapper.LocationMapper;
import cn.photo.po.Location;
import cn.photo.service.LocationService;

@Service
public class LocationServiceImpl implements LocationService {

	@Autowired
	private LocationMapper locationMapper;
	
	@Override
	public List<Location> queryAllLocation() {
		
		return locationMapper.queryAllLocation();
	}

}
