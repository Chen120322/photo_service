package cn.photo.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cn.photo.mapper.PhotoMapper;
import cn.photo.po.Photo;
import cn.photo.po.Photoalbum;
import cn.photo.service.PhotoService;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;

@Service
public class PhotoServiceImpl implements PhotoService {

	@Autowired
	private PhotoMapper photoMapper;
	
	@Override
	public PageInfo queryPhotoByAlbumId(Photoalbum photoalbum,int pageNum,int pageSize) {
		PageHelper.startPage(pageNum, pageSize, true);
		List<Photo> photos = photoMapper.queryPhotoByAlbumId(photoalbum);
		PageInfo page = new PageInfo(photos);
		return page;
	}

}
