package cn.photo.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cn.photo.mapper.BannerMapper;
import cn.photo.po.Banner;
import cn.photo.service.BannerService;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;

@Service
public class BannerServiceImpl implements BannerService {

	@Autowired
	private BannerMapper bannerMapper;
	
	@Override
	public PageInfo queryBanner(Integer pageNum, Integer pageSize) {
		PageHelper.startPage(pageNum, pageSize, true);
		List<Banner> banners = bannerMapper.queryBanner();
		PageInfo page = new PageInfo(banners);
		return page;
	}

}
