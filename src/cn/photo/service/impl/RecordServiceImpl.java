package cn.photo.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cn.photo.mapper.RecordMapper;
import cn.photo.po.Record;
import cn.photo.service.RecordService;
import cn.photo.utils.Sequence;

@Service
public class RecordServiceImpl implements RecordService {

	@Autowired
	RecordMapper rm;
	
	@Override
	public Integer insert(Record record) {
		Long id = (new Sequence()).nextId();
		record.setId(id);
		return rm.insert(record);
	}

	@Override
	public Integer queryPraise(Record record) {
		
		return rm.queryPraise(record);
	}

	@Override
	public Integer deletePraise(Record record) {
		// TODO Auto-generated method stub
		return rm.deletePraise(record);
	}

}
