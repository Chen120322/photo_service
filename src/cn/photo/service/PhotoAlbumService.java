package cn.photo.service;

import cn.photo.po.Photoalbum;

import com.github.pagehelper.PageInfo;

public interface PhotoAlbumService {
	
	public PageInfo queryCover(Integer pageNum,Integer pageSize,Photoalbum pa);

}
