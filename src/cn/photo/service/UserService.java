package cn.photo.service;

import java.util.List;

import com.github.pagehelper.PageInfo;

import cn.photo.po.Photoalbum;
import cn.photo.po.User;
import cn.photo.po.UserCustom;

/**
 * 用户信息service
 * @author Administrator
 *
 */
public interface UserService {

	//用户信息
	public UserCustom findUserInfo(UserCustom uc);
	
	public String findToken(String code) throws Exception;
	
	public String findUser(String token,String uid) throws Exception;
	
	public void saveUserInfo(UserCustom uc);
	
	/**
	 * 微信登录，获取用户信息，并返回登录态
	 * @return
	 * @throws Exception
	 */
	public String findWXLogin(String code) throws Exception;
	
	public void updateUserInfo(User user) throws Exception;
	
	/**
	 * 相册所属用户信息
	 * @param pa
	 * @return
	 */
	public User findPhotoUserInfo(Photoalbum pa);
	
	/**
	 * 排行榜用户信息
	 * @return
	 */
	public PageInfo findLastTopUserInfo(String time,Integer pageNum,Integer pageSize);
}
