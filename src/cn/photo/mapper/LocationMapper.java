package cn.photo.mapper;

import java.util.List;

import cn.photo.po.Location;

public interface LocationMapper {
	
	List<Location> queryAllLocation();

}
