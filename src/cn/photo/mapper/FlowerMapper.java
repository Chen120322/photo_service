package cn.photo.mapper;

import cn.photo.po.Flower;

public interface FlowerMapper {

    int deleteByPrimaryKey(Long id);

    int insert(Flower record);

    int insertSelective(Flower record);


    Flower selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(Flower record);

    int updateByPrimaryKey(Flower record);
}