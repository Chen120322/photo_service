package cn.photo.mapper;

import cn.photo.po.Record;

public interface RecordMapper {

    int deleteByPrimaryKey(Long id);

    int insert(Record record);

    int insertSelective(Record record);
    
    Record selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(Record record);

    int updateByPrimaryKey(Record record);
    
    int queryPraise(Record record);
    
    int deletePraise(Record record);
}