package cn.photo.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import cn.photo.po.Banner;

public interface BannerMapper {
   

    int deleteByPrimaryKey(Long id);

    int insert(Banner record);

    int insertSelective(Banner record);

   

    Banner selectByPrimaryKey(Long id);


    int updateByPrimaryKeySelective(Banner record);

    int updateByPrimaryKey(Banner record);
    
    List<Banner> queryBanner();
}