package cn.photo.mapper;

import java.util.List;

import cn.photo.po.Photo;
import cn.photo.po.Photoalbum;

public interface PhotoMapper {

    int deleteByPrimaryKey(Long id);

    int insert(Photo record);

    int insertSelective(Photo record);


    Photo selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(Photo record);

    int updateByPrimaryKey(Photo record);
    
    List<Photo> queryPhotoByAlbumId(Photoalbum photoalbum);
}