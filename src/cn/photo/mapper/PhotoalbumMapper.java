package cn.photo.mapper;

import java.util.List;

import cn.photo.po.Photoalbum;

public interface PhotoalbumMapper {

    int deleteByPrimaryKey(Long id);

    int insert(Photoalbum record);

    int insertSelective(Photoalbum record);

    Photoalbum selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(Photoalbum record);

    int updateByPrimaryKey(Photoalbum record);
    
    List<Photoalbum> queryCover(Photoalbum pa);
}