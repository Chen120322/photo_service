package cn.photo.mapper;

import java.util.List;

import cn.photo.po.Photoalbum;
import cn.photo.po.User;
import cn.photo.po.UserCustom;

public interface UserMapper {
  
    int deleteByPrimaryKey(Long id);

    int insert(UserCustom uc);

    int insertSelective(User record);

    User selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(User record);

    int updateByPrimaryKey(User record);
    
    UserCustom findUserInfo(UserCustom uc);
    
    User findPhotoUserInfo(Photoalbum pa);
    
    List<UserCustom> findLastTopUserInfo(String time);
}