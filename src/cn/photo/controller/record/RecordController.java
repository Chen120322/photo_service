package cn.photo.controller.record;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import cn.photo.core.BaseController;
import cn.photo.core.ReturnObject;
import cn.photo.po.Record;
import cn.photo.service.RecordService;

@Controller
@RequestMapping("/record")
public class RecordController extends BaseController{
	
	@Autowired
	RecordService rs;
	
	@RequestMapping("/saveRecord")
	public Object saveRecord(ModelMap modelMap,Long photoAlbum_id,Long user_id,Integer type){
		ReturnObject ro = new ReturnObject();
		Record r = new Record();
		r.setCreateTime(new Date());
		r.setPhotoalbumId(photoAlbum_id);
		r.setUserId(user_id);
		r.setType(type);
		int i = rs.insert(r);
		if(i == 0){
			ro.setSuccess(false);
			ro.setMessage("插入失败");
			return setSuccessModelMap(modelMap, ro);
		}
		ro.setSuccess(true);
		return setSuccessModelMap(modelMap, ro);
	}

	@RequestMapping("/queryPraise")
	public Object queryPraise(ModelMap modelMap,Long photoAlbum_id,Long user_id,Integer type){
		Map<String,Object> map = new HashMap<String,Object>();
		Record r = new Record();
		r.setPhotoalbumId(photoAlbum_id);
		r.setUserId(user_id);
		r.setType(type);
		int count = rs.queryPraise(r);
		map.put("count", count);
		return setSuccessModelMap(modelMap, map);
	}
	
	@RequestMapping("/deletePraise")
	public Object deletePraise(ModelMap modelMap,Long photoAlbum_id,Long user_id,Integer type){
		ReturnObject ro = new ReturnObject();
		Record r = new Record();
		r.setPhotoalbumId(photoAlbum_id);
		r.setUserId(user_id);
		r.setType(type);
		int i = rs.queryPraise(r);
		if(i != 0){
			i = rs.deletePraise(r);
			if(i == 0){
				ro.setSuccess(false);
				ro.setMessage("取消失败");
				return setSuccessModelMap(modelMap, ro);
			}
		}
		ro.setSuccess(true);
		return setSuccessModelMap(modelMap, ro);
	}
}
