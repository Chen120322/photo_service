package cn.photo.controller.location;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import cn.photo.core.BaseController;
import cn.photo.po.Location;
import cn.photo.service.LocationService;

@Controller
@RequestMapping("location")
public class LocationController extends BaseController{

	@Autowired
	private LocationService locationService;
	
	@RequestMapping("/queryAllLocation")
	public Object queryAllLocation(ModelMap modelMap){
		Map<String,Object> map = new HashMap<String,Object>();
		List<Location> list = locationService.queryAllLocation();
		map.put("list", list);
		return setSuccessModelMap(modelMap, map);
		
	}
}
