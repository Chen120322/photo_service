package cn.photo.controller.banner;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageInfo;

import cn.photo.core.BaseController;
import cn.photo.po.Banner;
import cn.photo.service.BannerService;

@Controller
@RequestMapping(value="/banner",method=RequestMethod.GET)

public class BannerController extends BaseController{

	@Autowired
	private BannerService bannerService;
	
	@RequestMapping(value="/queryBanner")
	public Object queryBanner(ModelMap modelMap,
			Integer pageNum,
			Integer pageSize){
		PageInfo banners = bannerService.queryBanner(pageNum,pageSize);
		
		return setSuccessModelMap(modelMap, banners);
	}
}
