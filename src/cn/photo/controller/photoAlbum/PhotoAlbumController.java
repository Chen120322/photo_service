package cn.photo.controller.photoAlbum;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import cn.photo.core.BaseController;
import cn.photo.po.Photoalbum;
import cn.photo.service.PhotoAlbumService;

import com.github.pagehelper.PageInfo;

@Controller
@RequestMapping("/photoAlbum")

public class PhotoAlbumController extends BaseController{
	
	@Autowired
	private PhotoAlbumService pbs;
	
	@RequestMapping("/queryCover")
	public Object queryCover(ModelMap modelMap,
			 @RequestParam(value = "pageNum",required = true) Integer pageNum,
			 @RequestParam(value = "pageSize",required = true) Integer pageSize,
			 @RequestParam(value="location",required = false) String location,
			 @RequestParam(value="name",required = false) String name){
		Photoalbum pa = new Photoalbum();
		pa.setLocation(location);
		pa.setName(name);
		PageInfo page =  pbs.queryCover(pageNum,pageSize,pa);
		
		return setSuccessModelMap(modelMap, page);
		
	}

}
