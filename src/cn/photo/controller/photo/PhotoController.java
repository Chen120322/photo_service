package cn.photo.controller.photo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import cn.photo.core.BaseController;
import cn.photo.po.Photoalbum;
import cn.photo.service.PhotoService;

import com.github.pagehelper.PageInfo;

@Controller
@RequestMapping("/photo")
public class PhotoController extends BaseController{
	
	@Autowired
	private PhotoService photoService;
	
	@RequestMapping("/queryPhotoByAlbumId")
	public Object queryPhotoByAlbumId(ModelMap modelMap,
			@RequestParam("photoAlbum_id") Long photoAlbum_id,
			@RequestParam("pageNum") Integer pageNum,
			@RequestParam("pageSize") Integer pageSize){
		Photoalbum photoalbum = new Photoalbum();
		photoalbum.setId(photoAlbum_id);
		PageInfo photos =  photoService.queryPhotoByAlbumId(photoalbum, pageNum, pageSize);
		return setSuccessModelMap(modelMap, photos);
	}
}
