package cn.photo.controller.user;

import java.io.File;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import net.sf.json.JSONObject;

import org.apache.commons.fileupload.disk.DiskFileItem;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.commons.CommonsMultipartFile;

import com.github.pagehelper.PageInfo;

import cn.photo.core.BaseController;
import cn.photo.core.ReturnObject;
import cn.photo.po.Photoalbum;
import cn.photo.po.User;
import cn.photo.po.UserCustom;
import cn.photo.service.UserService;
import cn.photo.utils.Constants;
import cn.photo.utils.DateUtil;
import cn.photo.utils.GetOpenid;
import cn.photo.utils.QiNiu;
import cn.photo.utils.SessionFactory;

/**
 * 用户信息controller
 * @author Administrator
 *
 */
@Controller
@RequestMapping("/user")

public class UserController extends BaseController{
	
	@Autowired
	private UserService us;
	
	/**
	 * 微博授权
	 * @param model
	 * @return
	 */
	@RequestMapping("/weiBoAuthorize")
	public String queryWeiBoAuthorize(Model model){
		Map<String,String> args = new HashMap<String,String>();
		args.put("client_id", Constants.WEIBO_APPID);
		args.put("redirect_uri",Constants.REDIRECT_URL);
		args.put("response_type", "code");
		args.put("action", Constants.WEIBO_URL+"oauth2/authorize");
		model.addAttribute("args", args);
		return "weibo";
	}
	
	/**
	 * 微博登录获取用户信息
	 * @param code
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/weiboUserInfo",method = RequestMethod.GET, produces = {"application/json;charset=UTF-8"})
	@ResponseBody
	public String queryWeiBoUserInfo(String code) throws Exception{
		JSONObject json = new JSONObject();
		String res = us.findToken(code);
		if(res == null){
			json.put("code", -1);
			return json.toString();
		}
		return res;
	}
	
	/**
	 * 微信登录，获取用户信息，并返回登录态
	 */
	@RequestMapping(value="/WXLogin",produces = {"application/json;charset=UTF-8"})
	@ResponseBody
	public String WXLogin(String code) throws Exception{
		
		String res = us.findWXLogin(code);
		System.out.println("数据："+res);
		
		return null;
	}
	
	
	@RequestMapping("/upload")
	public Object upload(ModelMap modelMap,HttpServletRequest request,
    		 @RequestParam(value="multfile",required=true) MultipartFile multfile){
    	ReturnObject ro = new ReturnObject();
    	
    	//原始名称  
        String originalFilename = multfile.getOriginalFilename();  

        //上传图片  
        if(multfile!=null && originalFilename!=null && originalFilename.length()>0){          
        	CommonsMultipartFile cf = (CommonsMultipartFile)multfile;  
        	DiskFileItem fi = (DiskFileItem) cf.getFileItem(); 
        	File file = fi.getStoreLocation();
	        QiNiu.uploadQiniu(file,originalFilename);
	        System.out.println(file.getName());
        }
    	ro.setSuccess(true);
    	return setSuccessModelMap(modelMap, ro);
    }
	
	@RequestMapping("/aaa")
	public Object aaa(ModelMap modelMap)throws Exception{
		ReturnObject ro = new ReturnObject();
		ro.setSuccess(true);
		return setSuccessModelMap(modelMap, ro);
	}
	
	/**
	 * 微信登录
	 * @param modelMap
	 * @param request
	 * @param code
	 * @param encryptedData
	 * @param iv
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/WXUserInfo",produces = {"application/json;charset=UTF-8"})
	public Object WXUserInfo(ModelMap modelMap,HttpServletRequest request,String code,String encryptedData,String iv) throws Exception{
		//调用工具类中获取session_key的方法
        String sessionkey = new GetOpenid().get(code);
        //调用工具类中的解密方法，然后返回给小程序就OK了
        JSONObject obj = new GetOpenid().getUserInfo(encryptedData, sessionkey,
                iv);
        Map<String,Object> map = new HashMap<String,Object>();
        Date date = new Date();
//        User user = new User(obj.getString("nickName"),"","",sessionkey,1,obj.getString("openId"),obj.getString("avatarUrl"),date,date);
        UserCustom uc = new UserCustom();
        uc.setName(obj.getString("nickName"));
        uc.setSessionKey(sessionkey);
        uc.setAvatarurl(obj.getString("avatarUrl"));
        uc.setCreateTime(date);
        uc.setUpdateTime(date);
		uc.setOpenid(obj.getString("openId"));
		//uc = us.findUserInfo(uc);
//		if(uc != null){
//			us.updateUserInfo(user);
//		}else{			
//			us.saveUserInfo(user);
//		}
		us.saveUserInfo(uc);
		uc = us.findUserInfo(uc);
		uc.setSessionKey("");
		uc.setOpenid("");
		HttpSession session = SessionFactory.getSession(request);
		String token = UUID.randomUUID().toString();
		String value = sessionkey+"&"+obj.getString("openId");
		session.setAttribute(token, value);
		session.setMaxInactiveInterval(60*60*24*30);//设置session的存放时间为30天
		map.put("token", token);
		map.put("user", uc);
		return setSuccessModelMap(modelMap, map);
	}
	
	@RequestMapping(value="/findUserInfo",produces = {"application/json;charset=UTF-8"})
	public Object findUserInfo(ModelMap modelMap,String token){
		Map<String,Object> map = new HashMap<String,Object>();
		UserCustom uc = new UserCustom();
		String openId = token.substring(token.lastIndexOf("&")+1,token.length());
		uc.setOpenid(openId);
		uc = us.findUserInfo(uc);
		map.put("user", uc);
		return setSuccessModelMap(modelMap, map);
	}
	
	@RequestMapping("/findPhotoUserInfo")
	public Object findPhotoUserInfo(ModelMap modelMap,Long photoAlbum_id){
		Map<String,Object> map = new HashMap<String,Object>();
		Photoalbum pa = new Photoalbum();
		pa.setId(photoAlbum_id);
		User user = us.findPhotoUserInfo(pa);
		map.put("user", user);
		return setSuccessModelMap(modelMap, map);
	}
	
	/**
	 * 本周排名榜用户信息
	 * @param modelMap
	 * @return
	 */
	@RequestMapping("/findLastTopUserInfo")
	public Object findLastTopUserInfo(ModelMap modelMap,Integer type,Integer pageNum,Integer pageSize){
		DateUtil date = new DateUtil();
		String time = "";
		switch (type) {
		case Constants.LASTWEEK:
			time = date.lastWeekDate();
			break;
		case Constants.LASTMONTH:
			time = date.lastMonthDate();
			break;
		default:
			break;
		}
		PageInfo list = us.findLastTopUserInfo(time,pageNum,pageSize);
		return setSuccessModelMap(modelMap, list);
	}
}
