package cn.photo.test;

import java.security.KeyFactory;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.Signature;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;
import java.util.ResourceBundle;

import javax.crypto.Cipher;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;

import sun.misc.BASE64Decoder;
import sun.misc.BASE64Encoder;
import cn.photo.mapper.UserMapper;

public class test {

	@Autowired
	private UserMapper um;
	public static String str_priK = "MIICdwIBADANBgkqhkiG9w0BAQEFAASCAmEwggJdAgEAAoGBAIGCCAIyRgi6BPe65chGYaFQNoMfvIIRPmay8gZkf+7ufavD3XkJVMc/JKZt0XKTKsmillWTJ3c7UtFHZtYUnRDKlurPDzC5XWBJFAXR0PPFeWmC3SxkYu+4fxtovId+ddi6cm6/BydwB3D2jr2GkNk36gjnQo/5OKC20Yvp+DCzAgMBAAECgYB++wHjNWvZrpqkU4ptniSnyEBKvNlWJpYqmBYaEnwlx41M1xTWUkHfwpeq305XqthvL417hndRvsbrD7lS+o8su0SEH+8eiLtcsMyDKWVdYNUr1r9KmCyliIkWUTTHiidPwbWxUnZovUn9ZwNkye14WTM3UzHg1gyKRfnd8RAJQQJBAMYFpMoT0u+nYy3vvDcmeoZAgFU55t/F41wAXwtEKxfak1Abtt0mizUXzewbLKxXbzQwSwG7ZgU8RgVeLKyxVFECQQCnbQvFwHdHHBt6m6ugGRCQbcJVzqzbBzScPlwiyEilDCLzQs93dZ7qphk7e4rfCzca5+XKReYbW4eAoYOsOsfDAkEAlwyhCgAbK/HMBrNTJ+JzHcs/2ULkqJcjef9SgJ6MHJL/QeESGRmYoHQ7ALot1nyPeAjKflS7Wo0CgH+4pjZSMQJBAJx+lwRu1I8JPL4sCA4Ln6oAjie6mWPZplltCJ4Sa6SqZXjQODBHJxXZz7gD0FiPmSBzJh0dnOpxcYTQc81m3BUCQGyglgqA2ZLnWb2thJozPE5YGCQqBaMwKWQxlLfo8LZe/107Iv5YsKASp+ZFUoDvx3hruyi/RbttxT//7fhQyCg=";

	public static String str_pubK = "MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCBgggCMkYIugT3uuXIRmGhUDaDH7yCET5msvIGZH/u7n2rw915CVTHPySmbdFykyrJopZVkyd3O1LRR2bWFJ0Qypbqzw8wuV1gSRQF0dDzxXlpgt0sZGLvuH8baLyHfnXYunJuvwcncAdw9o69hpDZN+oI50KP+TigttGL6fgwswIDAQAB";
	public static final String SIGNATURE_ALGORITHM="MD5withRSA";
	// 将base64编码后的公钥字符串转成PublicKey实例

	/**
	 * 使用getPublicKey得到公钥,返回类型为PublicKey
	 * 
	 * @param base64
	 *            String to PublicKey
	 * @throws Exception
	 */
	public static PublicKey getPublicKey(String key) throws Exception {
		byte[] keyBytes;
		keyBytes = (new BASE64Decoder()).decodeBuffer(key);
		X509EncodedKeySpec keySpec = new X509EncodedKeySpec(keyBytes);
		KeyFactory keyFactory = KeyFactory.getInstance("RSA");
		PublicKey publicKey = keyFactory.generatePublic(keySpec);
		return publicKey;
	}

	/**
	 * 转换私钥
	 * 
	 * @param base64
	 *            String to PrivateKey
	 * @throws Exception
	 */
	public static PrivateKey getPrivateKey(String key) throws Exception {
		byte[] keyBytes;
		keyBytes = (new BASE64Decoder()).decodeBuffer(key);
		PKCS8EncodedKeySpec keySpec = new PKCS8EncodedKeySpec(keyBytes);
		KeyFactory keyFactory = KeyFactory.getInstance("RSA");
		PrivateKey privateKey = keyFactory.generatePrivate(keySpec);
		return privateKey;
	}

	// ***************************签名和验证*******************************
	public static byte[] sign(byte[] data) throws Exception {
		PrivateKey priK = getPrivateKey(str_priK);
		Signature sig = Signature.getInstance(SIGNATURE_ALGORITHM);
		sig.initSign(priK);
		sig.update(data);
		return sig.sign();
	}

	public static boolean verify(byte[] data, byte[] sign) throws Exception {
		PublicKey pubK = getPublicKey(str_pubK);
		Signature sig = Signature.getInstance(SIGNATURE_ALGORITHM);
		sig.initVerify(pubK);
		sig.update(data);
		return sig.verify(sign);
	}

	// ************************加密解密**************************
	public static byte[] encrypt(byte[] bt_plaintext) throws Exception {
		PublicKey publicKey = getPublicKey(str_pubK);
		Cipher cipher = Cipher.getInstance("RSA");
		cipher.init(Cipher.ENCRYPT_MODE, publicKey);
		byte[] bt_encrypted = cipher.doFinal(bt_plaintext);
		return bt_encrypted;
	}

	public static byte[] decrypt(byte[] bt_encrypted) throws Exception {
		PrivateKey privateKey = getPrivateKey(str_priK);
		Cipher cipher = Cipher.getInstance("RSA");
		cipher.init(Cipher.DECRYPT_MODE,  privateKey);
		byte[] bt_original = cipher.doFinal(bt_encrypted);
		return bt_original;
	}

	// ********************main函数：加密解密和签名验证*********************
	public static void main(String[] args) throws Exception {
		String str_plaintext = "这是一段用来测试密钥转换的明文";
		System.err.println("明文：" + str_plaintext);
		byte[] bt_cipher =  decrypt(str_plaintext.getBytes());
		//System.out.println("加密后：" + Base64.encodeBase64String(bt_cipher));
		System.out.println(new BASE64Encoder().encode(bt_cipher));
		byte[] bt_original = encrypt(bt_cipher);
		String str_original = new String(bt_original);
		System.out.println("解密结果:" + str_original);

		String str = "被签名的内容";
		System.err.println("\n原文:" + str);
		byte[] signature = sign(str.getBytes());
		//System.out.println("产生签名：" + Base64.encodeBase64String(signature));
		boolean status = verify(str.getBytes(), signature);
		System.out.println("验证情况：" + status);
	}
	
	@Test
	public void aaa(){
		String str = "ev8fxoyhAEo4d+wVywNFM8+RtawKNTDRdKdlZhyYjkuZPcPiVjqy+wwPhafxFE3In35rL2aqUqZ+sr/9Np8sqjX9eBdFqg+wERY0LGXzZ1byXEFs/cOpjmejYHrWgOcAmqnzDw+FL8WMREGuycxCAndj21rQZ7tuvr4wGUV2xMg=";
		byte[] byteArray = str.getBytes();
		try {
			byte[] bt_original = decrypt(byteArray);
			String str_original = new String(bt_original);
			System.out.println(str_original);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@Test
	public void aaaq() throws Exception{
		String s = "6WRcXd4ML1qQZ5FrBAMv7w==";
		BASE64Decoder d = new BASE64Decoder();
		String ss = new String(d.decodeBuffer(s));
		System.out.println(ss);
	}
	
	@Test
	public void qq(){
		Locale locale = LocaleContextHolder.getLocale();
		//设置本土化显示国家语言
		ResourceBundle bundle = ResourceBundle.getBundle("message", locale);
		//此类的作用是通过ResourceBundle类来进行资源文件的绑定
		String value = bundle.getString("hello");
		//从资源文件中通过键拿到值
		System.out.println(value);
		 
	}
	
	@Test
	//上个月
	public void date(){
		Calendar c = Calendar.getInstance();
		  c.add(Calendar.MONTH, -1);
		  SimpleDateFormat format =  new SimpleDateFormat("yyyy-MM");
		  String time = format.format(c.getTime());
		  System.out.println(time);
	}
	//本周一
	@Test
	public void dateDay(){
		Calendar cal =Calendar.getInstance();
	    cal.set(Calendar.DAY_OF_WEEK,1);
	   
	    SimpleDateFormat format =  new SimpleDateFormat("yyyy-MM-dd");
	    String time = format.format(cal.getTime());
	    System.out.println(time);
	}
}
