package cn.photo.callback;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import cn.photo.core.BaseController;
import cn.photo.core.ReturnObject;

@Controller
@RequestMapping("/callback")
public class Callback extends BaseController{
	
	@RequestMapping("/uploadCallback")
	public Object uploadCallback(ModelMap modelMap){
		ReturnObject ro = new ReturnObject();
		ro.setSuccess(true);
		return setSuccessModelMap(modelMap, ro);
	}

}
